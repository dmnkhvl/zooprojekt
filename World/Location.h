#ifndef ZOOPROJEKT_LOCATION_H
#define ZOOPROJEKT_LOCATION_H
#include <iostream>

class Location {
    std::string m_popis;
public:
    // Konstruktor
    Location(std::string popis);

    std::string getPopis();

    //Destruktor - zatial nepouzity
    virtual ~Location();
};


#endif //ZOOPROJEKT_LOCATION_H
