//
// Created by Dominik Havel on 07/12/2021.
//

#include "Location.h"

Location::Location(std::string popis) {
    if (popis == ""){
        std::cerr << "HraciPole::HraciPole - snazite se do popisu pole nastavit prazdnou hodnotu" << std::endl;
    }
    m_popis = popis;

}

std::string Location::getPopis() {
    return m_popis;
}
